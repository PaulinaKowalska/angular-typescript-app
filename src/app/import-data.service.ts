import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ImportDataService {
  constructor(private http: HttpClient) {}

  fetchLessData(): Observable<object> {
    return this.http.get('http://localhost:8000/api/1k-rows');
  }

  fetchMoreData(): Observable<object>  {
    return this.http.get('http://localhost:8000/api/10k-rows');
  }

}
