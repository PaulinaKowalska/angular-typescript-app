import { Component } from '@angular/core';
import { ImportDataService } from './import-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  lessDataLoaded$;
  moreDataLoaded$;
  constructor(private importDataService: ImportDataService) { }

  fetchLessData() {
    this.lessDataLoaded$ = this.importDataService.fetchLessData();
  }

  fetchMoreData() {
    this.moreDataLoaded$ = this.importDataService.fetchMoreData();
  }

}
